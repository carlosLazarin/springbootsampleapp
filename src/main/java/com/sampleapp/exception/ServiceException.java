package com.sampleapp.exception;

/**
 * 
 * @author Lazarin, Carlos
 *
 */
public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1578622910836049037L;

	public ServiceException(String errorMsg) {
		super(errorMsg);
	}
}