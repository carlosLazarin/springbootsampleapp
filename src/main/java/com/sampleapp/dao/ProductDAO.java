package com.sampleapp.dao;

import com.sampleapp.entity.Product;

/**
 * 
 * @author Lazarin, Carlos
 *
 */
public interface ProductDAO extends BaseDAO<Product> {
	

}