package com.sampleapp.dao;

import java.util.List;

import com.sampleapp.entity.Image;

/**
 * 
 * @author Lazarin, Carlos
 *
 */
public interface ImageDAO extends BaseDAO<Image> {
	
	List<Image> findImagesByProductId(Long productId);

} 