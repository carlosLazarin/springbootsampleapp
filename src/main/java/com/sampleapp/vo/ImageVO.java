package com.sampleapp.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Lazarin, Carlos
 *
 */
@Getter
@Setter
@ToString
public class ImageVO implements Serializable{

	private static final long serialVersionUID = 4789859045437504662L;

	private Long id;

	private String type;
	
	private Long productId;
}