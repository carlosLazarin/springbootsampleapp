package com.sampleapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sampleapp.exception.ServiceException;
import com.sampleapp.service.ProductService;
import com.sampleapp.vo.ProductVO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Lazarin,Carlos
 */
@RestController
@RequestMapping("/product")
public class ProductRestController {

	private ProductService productService;

	private static final String PRODUCT_CREATED_SUCCESSFULLY_MSG = "New product created successfully";
	private static final String PRODUCT_DELETED_SUCCESSFULLY_MSG = "Product deleted successfully";
	private static final String PRODUCT_UPDATED_SUCCESSFULLY_MSG = "Product updated successfully";

	@Autowired
	public ProductRestController(ProductService productService) {
		this.productService = productService;
	}

	@ApiOperation(value = "Creates a new product for given payload", response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 201, message = PRODUCT_CREATED_SUCCESSFULLY_MSG),
	})
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createProduct(@RequestBody ProductVO productVO) throws ServiceException {
		productService.createNewProduct(productVO);
		return new ResponseEntity<String>(PRODUCT_CREATED_SUCCESSFULLY_MSG, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Updates product for given payload", response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = PRODUCT_UPDATED_SUCCESSFULLY_MSG),
	})
	@RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateProduct(@RequestBody ProductVO productVO) {
		productService.updateProduct(productVO);
		return new ResponseEntity<String>(PRODUCT_UPDATED_SUCCESSFULLY_MSG, HttpStatus.OK);
	}

	@ApiOperation(value = "Deletes product for given id", response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = PRODUCT_DELETED_SUCCESSFULLY_MSG),
	})
	@RequestMapping(method = RequestMethod.DELETE, value = "delete/{productId}")
	public ResponseEntity<String> deleteProduct(@PathVariable Long productId){
    	productService.deleteProduct(productId);
		return new ResponseEntity<String>(PRODUCT_DELETED_SUCCESSFULLY_MSG, HttpStatus.OK);
	}

	@ApiOperation(value = "List all products - you might either include relationships (images + parent) or not",responseContainer = "List", response=ProductVO.class)
	@RequestMapping(value = "allproducts/{includeRelationship}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductVO>> getAllProducts(@PathVariable boolean includeRelationship) {
		return new ResponseEntity<List<ProductVO>>(this.productService.findAllProducts(includeRelationship), HttpStatus.OK);
	}

	@ApiOperation(value = "List product details - you might either include relationships (images + parent) or not",responseContainer = "List", response=ProductVO.class)
	@RequestMapping(value = "details/{includeRelationship}/{productId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductVO> getProductDetails(@PathVariable boolean includeRelationship, @PathVariable Long productId) throws ServiceException {
		return new ResponseEntity<ProductVO>(this.productService.findProductById(productId,includeRelationship), HttpStatus.OK);
	}

	@ApiOperation(value = "List of children products for given product parent id",responseContainer = "List", response=ProductVO.class)
	@RequestMapping(value = "child/{parentId}", method = RequestMethod.GET)
	public ResponseEntity<List<ProductVO>> getChildrenProducts(@PathVariable Long parentId) throws ServiceException {
		return new ResponseEntity<List<ProductVO>>(this.productService.findChildrenForParentId(parentId), HttpStatus.OK);
	}

}
