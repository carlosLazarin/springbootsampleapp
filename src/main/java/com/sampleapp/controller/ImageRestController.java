package com.sampleapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sampleapp.exception.ServiceException;
import com.sampleapp.service.ImageService;
import com.sampleapp.vo.ImageVO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Lazarin,Carlos
 */
@RestController
@RequestMapping("/image")
public class ImageRestController {
	
	private ImageService imageService;
	
	private static final String IMAGE_CREATED_SUCCESSFULLY_MSG = "New image created successfully";
	private static final String IMAGE_DELETED_SUCCESSFULLY_MSG = "Image deleted successfully";
	private static final String IMAGE_UPDATED_SUCCESSFULLY_MSG = "Image updated successfully";
	
	@Autowired
	public ImageRestController(ImageService imageService) {
		this.imageService = imageService;
	}
	
	@ApiOperation(value = "Creates a new image", response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 201, message = IMAGE_CREATED_SUCCESSFULLY_MSG),
	})
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createImage(@RequestBody ImageVO imageVO) throws ServiceException {
		this.imageService.createImage(imageVO);
		return new ResponseEntity<String>(IMAGE_CREATED_SUCCESSFULLY_MSG, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Updates image for given payload", response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = IMAGE_UPDATED_SUCCESSFULLY_MSG),
	})
	@RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateImage(@RequestBody ImageVO imageVO) {
		this.imageService.updateImage(imageVO);
		return new ResponseEntity<String>(IMAGE_UPDATED_SUCCESSFULLY_MSG, HttpStatus.OK);
	}

	@ApiOperation(value = "Deletes image for given id", response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = IMAGE_DELETED_SUCCESSFULLY_MSG),
	})
	@RequestMapping(method = RequestMethod.DELETE, value = "delete/{imageId}")
	public ResponseEntity<String> deleteImage(@PathVariable Long imageId){
		this.imageService.deleteImage(imageId);
		return new ResponseEntity<String>(IMAGE_DELETED_SUCCESSFULLY_MSG, HttpStatus.OK);
	}
	
	@ApiOperation(value = "List of images for given product id",responseContainer = "List", response=ImageVO.class)
	@RequestMapping(value = "list/{productId}", method = RequestMethod.GET)
	public ResponseEntity<List<ImageVO>> getProductImages(@PathVariable Long productId) throws ServiceException {
		return new ResponseEntity<List<ImageVO>>(this.imageService.findImagesForProductId(productId), HttpStatus.OK);
	}
}