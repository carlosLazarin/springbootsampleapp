package com.sampleapp.service;

import java.util.List;

import com.sampleapp.entity.Image;
import com.sampleapp.exception.ServiceException;
import com.sampleapp.vo.ImageVO;

/**
 * 
 * @author Lazarin, Carlos
 *
 */
public interface ImageService {

	void createImage(ImageVO imageVO) throws ServiceException;
	
	void deleteImage(Long imageId);
	
	void updateImage(ImageVO imageVO);
	
	List<Image> findAllImages();
	
	void deleteAllImages();
	
	Image findImageById(Long imageId) throws ServiceException;
	
	List<ImageVO> findImagesForProductId(Long productId) throws ServiceException;
}
