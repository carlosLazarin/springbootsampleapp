package com.sampleapp.service;

import java.util.List;

import com.sampleapp.entity.Product;
import com.sampleapp.exception.ServiceException;
import com.sampleapp.vo.ProductVO;

/**
 * 
 * @author Lazarin, Carlos
 *
 */
public interface ProductService {
	
	void createNewProduct(ProductVO productVO) throws ServiceException;
	
	void deleteProduct(Long productId);
	
	void updateProduct(ProductVO productVO);
	
	ProductVO findProductById(Long productId, boolean includeRelationship) throws ServiceException;
	
	List<ProductVO> findAllProducts(boolean includeRelationship);
	
	List<ProductVO> findChildrenForParentId(Long parentId) throws ServiceException;
	
	void deleteAllProducts();
	
	Product findProductById(Long productId) throws ServiceException;
}