package com.sampleapp.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;
import com.sampleapp.service.ImageService;
import com.sampleapp.vo.ImageVO;

@RunWith(SpringRunner.class)
@WebMvcTest(ImageRestController.class)
public class ImageRestControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
    private ImageService imageService;

	private String jsonPayload() {
		ImageVO stub = new ImageVO();
		Gson gson = new Gson();
	    return gson.toJson(stub);
	}

	@Test
	public void testCreateProduct() throws Exception {
		this.mockMvc.perform(
				post("/image/create")
				.content(this.jsonPayload())
				.contentType(MediaType.APPLICATION_JSON)
				)
		.andDo(print())
		.andExpect(status()
				.isCreated())
		.andExpect(content()
				.string(containsString("New image created successfully")));
	}
	
	@Test
	public void testUpdateProduct() throws Exception {
		this.mockMvc.perform(
				put("/image/update")
				.content(this.jsonPayload())
				.contentType(MediaType.APPLICATION_JSON)
				)
		.andDo(print())
		.andExpect(status()
				.isOk())
		.andExpect(content()
				.string(containsString("Image updated successfully")));
	}
	
	@Test
	public void testDeleteProduct() throws Exception {
		this.mockMvc.perform(
				delete("/image/delete/1")
				)
		.andDo(print())
		.andExpect(status()
				.isOk())
		.andExpect(content()
				.string(containsString("Image deleted successfully")));
	}
	
}
